class AddAttachmentPicturesToSlides < ActiveRecord::Migration
  def self.up
    change_table :slides do |t|
      t.attachment :pictures
    end
  end

  def self.down
    remove_attachment :slides, :pictures
  end
end
