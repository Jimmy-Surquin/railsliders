class AddDescToSlide < ActiveRecord::Migration
  def change
    add_column :slides, :desc_left, :text
    add_column :slides, :desc_right, :text
  end
end
