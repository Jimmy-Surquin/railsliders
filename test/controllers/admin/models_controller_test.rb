require 'test_helper'

class Admin::ModelsControllerTest < ActionController::TestCase
  setup do
    @model = models(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:models)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create model" do
    assert_difference('Model.count') do
      post :create, model: { desc_left: @model.desc_left, desc_right: @model.desc_right, name: @model.name, pictures: @model.pictures, slides: @model.slides }
    end

    assert_redirected_to admin_model_path(assigns(:model))
  end

  test "should show model" do
    get :show, id: @model
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @model
    assert_response :success
  end

  test "should update model" do
    patch :update, id: @model, model: { desc_left: @model.desc_left, desc_right: @model.desc_right, name: @model.name, pictures: @model.pictures, slides: @model.slides }
    assert_redirected_to admin_model_path(assigns(:model))
  end

  test "should destroy model" do
    assert_difference('Model.count', -1) do
      delete :destroy, id: @model
    end

    assert_redirected_to admin_models_path
  end
end
