class Slide < ActiveRecord::Base
  validates :name, presence: true
  has_attached_file :pictures, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :pictures, content_type: /\Aimage\/.*\z/
end
