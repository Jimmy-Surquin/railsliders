class Admin::ModelsController < ApplicationController
  before_action :set_model, only: [:show, :edit, :update, :destroy]

  # GET /admin/models
  # GET /admin/models.json
  def index
    @models = Model.all
  end

  # GET /admin/models/1
  # GET /admin/models/1.json
  def show
  end

  # GET /admin/models/new
  def new
    @model = Model.new
  end

  # GET /admin/models/1/edit
  def edit
  end

  # POST /admin/models
  # POST /admin/models.json
  def create
    @model = Model.new(model_params)

    respond_to do |format|
      if @model.save
        format.html { redirect_to [:admin, @model], notice: 'Model was successfully created.' }
        format.json { render action: 'show', status: :created, location: @model }
      else
        format.html { render action: 'new' }
        format.json { render json: @model.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/models/1
  # PATCH/PUT /admin/models/1.json
  def update
    respond_to do |format|
      if @model.update(model_params)
        format.html { redirect_to [:admin, @model], notice: 'Model was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @model.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/models/1
  # DELETE /admin/models/1.json
  def destroy
    @model.destroy
    respond_to do |format|
      format.html { redirect_to admin_models_url, notice: 'Model was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_model
      @model = Model.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def model_params
      params.require(:model).permit(:slides, :name, :desc_left, :desc_right, :pictures)
    end
end
