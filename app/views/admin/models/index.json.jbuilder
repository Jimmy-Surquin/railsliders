json.array!(@models) do |model|
  json.extract! model, :slides, :name, :desc_left, :desc_right, :pictures
  json.url model_url(model, format: :json)
end